<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'simplysecure_mage_');

/** MySQL database username */
define('DB_USER', 'simplysecure');

/** MySQL database password */
define('DB_PASSWORD', 'simplypassword');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0ODv]{)Lhy=`T!y#{1(F-hun|]:qYgG0@f{~+()KW$lkU1X?e5cgxIxAdOh ql ]');
define('SECURE_AUTH_KEY',  ';csEAM5cBb{IHbwCWJv:UYRFg4s3:! ?X7#q&Ky@OMkvu:)+JN^J];ol.@5On8Z;');
define('LOGGED_IN_KEY',    'L&^rn wm;SpvFcb.lLO!$JWtDs&O~y5i}3h=T&^kG)0lB48pE?zwi$=BxpS/[:ic');
define('NONCE_KEY',        'vwpn4[C;MN_@TS@^|^cno324k*>}uJ]o7<`BlBsJ$~/P#lP?q2O=kmdrs=o]G7O<');
define('AUTH_SALT',        'FMLVnQO|iJZ`|w04L2AT:NlSs6kn&XK(*HrSVfQUi198c_ZQ|6L^{Uvrv^Ked+EG');
define('SECURE_AUTH_SALT', 'XqAKay21~KmDzy|i)%XkN^qgHc$29wU5mrKf]o:<2%=O~NK!$xJrU$[=;f!NG?3R');
define('LOGGED_IN_SALT',   '&V-liK<o3-h7O<*SIkTgEmEs9[GM1k>.k_y45GJ[pVQL#B#O//TuaKJrP]_2)vca');
define('NONCE_SALT',       'm4f #gb)7V80cr>#u:?lntEq5d=2^&fb<B#s3ojqQ0zff+o^M&5;0b? gQkqOGkC');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
