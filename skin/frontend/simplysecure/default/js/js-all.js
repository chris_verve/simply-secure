// Magento uses Prototype as the main JavaScript library. To make jQuery
// work correctly without conflicts, you need to insert the no conflict code
jQuery(document).ready(function( $ ) {
// -------------------------------- //
// $(window).load(function () {
// -------------------------------- //
	
	$('p:empty').remove();


	/* Cart Area */
	$('.cart-area-normal').hover(function() {
		$(this).find('.cart-area-hovered').stop(true, true).fadeIn();
	}, function() {
		$(this).find('.cart-area-hovered').stop(true, true).fadeOut();
	});
	
	
	
	/* Mega Menu */
	$('.n-shop').hover(function() {
		// $(this).addClass('n-hover');
		// $('.nmain').find('.submain').stop(true, true).show();
	}, function() {
		// $('.nmain').find('.submain').stop(true, true).fadeOut();
	});	
	$(".submain").mouseleave(function(){
        // $(this).hide();
		// $('.n-shop').removeClass('n-hover');
    });
	/* // Mega Menu */
	
	
	
	 
 
	
	/* Integrates Bootstrap 3 Multi-level Menu
	
	$('.navbar .parent').addClass('dropdown');
	$('.navbar a.level-top').addClass('dropdown-toggle');
	$('.navbar li.parent ul').addClass('dropdown-menu');
	 
	$('.navbar li.level1 ul').prepend('<li class="dropdown-submenu">');
	
	$('.navbar ul.nav li.level0.dropdown').hover(function() {
		$(this).find('.level0.dropdown-menu').stop(true, true).fadeIn();
	}, function() {
		$(this).find('.level0.dropdown-menu').stop(true, true).fadeOut();
	});
	
	$('.navbar ul.nav li.level0.dropdown li.level1.dropdown').hover(function() {
		$(this).find('.level1.dropdown-menu').stop(true, true).fadeIn();
	}, function() {
		$(this).find('.level1.dropdown-menu').stop(true, true).fadeOut();
	});
	
	 */
	
 

    /* Navigation
    jQuery("ul#timbernav > li.collector").hover(function(){
        jQuery(this).addClass("hover");
        jQuery('ul:first',this).css('display', 'block');
    }, function(){
        jQuery(this).removeClass("hover");
        jQuery('ul:first',this).css('display', 'none');
    });
    jQuery("ul#nav li ul li:has(ul)").find(" span:first ").append(" &raquo; ");
     */

 

    /*
    jQuery("ul#uppernav").tinyNav({
        active: 'selected', // String: Set the "active" class
        header: '- NAVIGATION -', // String: Specify text for "header" and show header instead of the active item
        indent: '- ', // String: Specify text for indenting sub-items
        label: '' // String: Sets the <label> text for the <select> (if not set, no label will be added)
    });
    */
	
	
	
	// mobile-nav-primary
	$( "div.clickable-mobile > ul" ).addClass( "mob-1" );
	$( "div.clickable-mobile > ul > li > ul" ).addClass( "mob-2" );
	$( "div.clickable-mobile > ul > li > ul > li > ul" ).addClass( "mob-3" );
	$( "div.clickable-mobile > ul > li > ul > li > ul > li > ul" ).addClass( "mob-4" );
	$( "div.clickable-mobile > ul > li > ul > li > ul > li > ul > li > ul" ).addClass( "mob-5" );
	
	// HIDES THE SUBMENUS ON LOAD
    $('ul.mob-2, ul.mob-3, ul.mob-4, ul.mob-5').hide();
	
	// CHECK FOR CHILDREN MUST RUN IN ONCLICK FUNCTION, OTHERWISE IT WILL ALWAYS RETURN TRUE
	// Level
 	$('ul.mob-1 > li > a').click(function(e) {
 	    if($(this).parent().has('ul.mob-2')){
 	        e.preventDefault();
            $('ul.mob-2', $(this).parent()).toggle();
        }
    });
	
	
	$("#mobile-menu").mobileMenu({
		MenuWidth: '100%',
		WindowsMaxWidth : 767,
		CollapseMenu : true,
		ClassName : "mobile-menu"
	});
	
 
 

// -------------------------------- //
// }); //$(window).load(function () {	 
// -------------------------------- //
/*})(jQuery);*/
});