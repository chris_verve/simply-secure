jQuery(document).ready(function( $ ) {
/* -----------------------------------------------------------------------------------*/


// PRODUCT TABS

/** Hide all **/
$('#tttabs li:not(:first)').addClass('inactive');
$('.ttt-container').hide();
/* End Hide all **/
/** Show first **/
$('.ttt-container:first').show();
$('#tttabs li:first').addClass('active');
/** End show first **/

interval = setInterval(function($) {
	 t = jQuery('#tttabs .active').attr('id'); // get the id of the nav element

	 if(jQuery('#tttabs .active').next('#tttabs li').hasClass('inactive'))
	 {
		jQuery('#'+t).removeClass('active');
		jQuery('#tttabs li').addClass('inactive');
		jQuery('#'+t).next('#tttabs li').removeClass('inactive');
		jQuery('#'+t).next('#tttabs li').addClass('active');
		//$(this).next('#tttabs li').addClass('active');
		jQuery('.ttt-container').hide();
		jQuery('#'+ jQuery('#'+t).next('#tttabs li').attr('id')+ 'C').fadeIn('slow');
	 }else
	 {
		jQuery('#'+t).removeClass('active');
		jQuery('#tttabs li').addClass('inactive');
		jQuery('#tttabs li').first().removeClass('inactive');
		jQuery('#tttabs li').first().addClass('active');
		//$(this).next('#tttabs li').addClass('active');
		jQuery('.ttt-container').hide();
		jQuery('#'+ jQuery('#tttabs li').first().attr('id')+ 'C').fadeIn('slow');
	 }
}, 105000);  // 5000

$('#tttabs li').click(function(){

  var t = $(this).attr('id'); // get the id of the nav element

  /** if($(this).hasClass('inactive')){ //this is the start of our condition
	$('#tttabs li').addClass('inactive');
	$(this).removeClass('inactive');

	$('.ttt-container').hide();
	$('#'+ t + 'C').fadeIn('slow');
 }**/

 if($(this).hasClass('inactive'))
 {
	$('#tttabs li').removeClass('active');
	$('#tttabs li').addClass('inactive');
	$(this).removeClass('inactive');
	$(this).addClass('active');
	//$(this).next('#tttabs li').addClass('active');
	$('.ttt-container').hide();
	$('#'+ t + 'C').fadeIn('slow');
 }
 //setTimeout(rotateBanners, 5000);
});
// END | PRODUCT TABS

//$('#tttabs li:first').click();
// END | PRODUCT TABS



/*
$('.carousel-products').jCarouselLite({
  // https://github.com/kswedberg/jquery-carousel-lite
  // autoCSS: false,
  autoWidth: true,
  responsive: true,
  btnNext: '.go-next',
  btnPrev: '.go-prev',
  visible: 4 
});
*/


// http://www.owlcarousel.owlgraphic.com/docs/api-options.html
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true,
			navText : ['','']
        },
        768:{
            items:2,
            nav:true,
			navText : ['','']
        },
        992:{
            items:3,
            nav:true,
			navText : ['','']
        },
        1200:{
            items:4,
            nav:true,
			navText : ['','']
        }
    }
});
 


/* -----------------------------------------------------------------------------------*/
});