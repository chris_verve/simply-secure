// Magento uses Prototype as the main JavaScript library. To make jQuery
// work correctly without conflicts, you need to insert the no conflict code
jQuery(document).ready(function( $ ) {
/* -----------------------------------------------------------------------------------*/
 
// Bind to the resize event of the window object
jQuery(window).on("resize", function () { 
	if (jQuery(window).width() < 752) { // 991 
	   jQuery("aside.col-left.sidebar").detach().insertAfter("div.col-main")
	}
	else {
		jQuery("div.col-main").detach().insertAfter("aside.col-left.sidebar")
	}
// Invoke the resize event immediately
}).resize();

/* -----------------------------------------------------------------------------------*/
});