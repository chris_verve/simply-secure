// Magento uses Prototype as the main JavaScript library. To make jQuery
// work correctly without conflicts, you need to insert the no conflict code
jQuery(document).ready(function( $ ) {
/* -----------------------------------------------------------------------------------*/

/* easyResponsiveTabs.js
https://github.com/samsono/Easy-Responsive-Tabs-to-Accordion */
jQuery("#productViewTabs").easyResponsiveTabs({
	type: 'default', //Types: default, vertical, accordion
	width: 'auto', //auto or any custom width
	fit: true,   // 100% fits in a container
	closed: 'tabs', // accordion // tabs // false // Close the panels on start, the options 'accordion' and 'tabs' keep them closed in there respective view types
	activate: function() {}  // Callback function, gets called if tab is switched
});

/* http://lab.veno.it/venobox/ */
jQuery('a.product-image , .more-views>a ').venobox({
	border: '3px',             	// default: '0'
	bgcolor: '#000',         	// default: '#fff'
	titleattr: 'title',    		// default: 'title' or 'data-title'
	numeratio: true,            // default: false
	infinigall: true            // default: false
}); 

/* -----------------------------------------------------------------------------------*/
});